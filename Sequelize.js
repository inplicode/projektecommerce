const Sequelize = require('sequelize');
const WebsiteTestModel = require('./models/WebsiteTest');

//https://www.codementor.io/mirko0/how-to-use-sequelize-with-node-and-express-i24l67cuz

//connection to database
const sequelize = new Sequelize('projektecommerce', 'db_hightech', 'DB_HighTech9876', {
  host: 'localhost',
  dialect: 'mysql'
});

const WebsiteTest = WebsiteTestModel(sequelize, Sequelize);




sequelize.sync({ force: true })
  .then(() => {
    console.log(`Database & tables created!`)
  });



module.exports = { 
  Sequelize,
  sequelize,
  WebsiteTest
};