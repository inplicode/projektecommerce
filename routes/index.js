var express = require('express');
var router = express.Router();
const pagespeed = require('gpagespeed');
const parseDomain = require('parse-domain');

//const { WebsiteTest } = require('../Sequelize');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', {
    title: "Zaloguj się"
  });
});



router.post('/', async function(req, res, next) {


  const website = req.body.website;

  var parse = parseDomain(website);
  console.log(JSON.stringify(parse));

  const websiteParse = 'https://' + parse.domain + '.' + parse.tld;


  const options = {
    url: websiteParse,
    key: 'AIzaSyCPMLCEjYTNmYkmhL-15aAXzbwqpwHqr-U'
  };
   
  pagespeed(options)
    .then(data => {
      console.log(data);

      res.render('index', {
        websitetest: true,
        data: data
      });

    })
    .catch(error => {
        
        console.log(error);
        res.render('index', {
        websitetest: true,
        error: error
      });
    });

});



/* GET home page. */
router.get('/omnie', function(req, res) {
  res.render('aboutme.ejs', {
    title: "Zaloguj się"
  });
});

/* GET home page. */
router.get('/uslugi', function(req, res) {
  res.render('service.ejs', {
    title: "Zaloguj się"
  });
});

/* GET home page. */
router.get('/politykaprywatnosci', function(req, res) {
  res.render('service.ejs', {
    title: "Zaloguj się"
  });
});

/* GET home page. */
router.get('/politykacookies', function(req, res) {
  res.render('service.ejs', {
    title: "Zaloguj się"
  });
});

/* GET home page. */
router.get('/kontakt', function(req, res) {
  res.render('contact', {
    title: "Zaloguj się"
  });
});

module.exports = router;
