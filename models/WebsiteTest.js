module.exports = (sequelize, type) => {
  return sequelize.define('WebsiteTest', {
      website: {
        type: type.STRING,
        allowNull: false
      }
  }, {
    charset: 'utf8',
    collate: 'utf8_polish_ci'
  });
}